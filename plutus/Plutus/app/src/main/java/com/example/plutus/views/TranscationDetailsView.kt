package com.example.plutus.views

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.plutus.NavigationRoutes
import com.example.plutus.utils.values.TextSizeValue
import com.example.plutus.utils.*
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.CustomColors
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import com.example.plutus.utils.values.PaddingValue

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TransactionDetailsView(transaction: Transaction, onClickReturn: () -> Unit) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(PaddingValue.SIMPLE.dp)
    ) {
        Row(Modifier.fillMaxWidth()) {
            Text(text = transaction.name, fontSize = TextSizeValue.SUB_TITLE_1.sp)
            Text(
                text = String.format("%.2f", transaction.amount),
                fontSize = TextSizeValue.SUB_TITLE_1.sp,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.End
            )
        }
        Text(text = transaction.date)
        LazyRow(Modifier.padding(2.dp)) {
            items(items = transaction.tags.toList(), itemContent = { tag ->
                TagTile(tag = tag, 2)
            })
        }
        Card(
            backgroundColor = CustomColors.LighterGray,
            shape = RoundedCornerShape(8.dp),
            modifier = Modifier.padding(8.dp),
            elevation = 3.dp
        ) {
            Text(
                text = transaction.text,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp)
                    .padding(PaddingValue.SIMPLE.dp / 2)
            )
        }
        Button(onClick = { onClickReturn() }) {
            Text(text = "Retour")
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyTransactionDetailsPreview() {
    var transaction = Transaction.getDefault()
    var t1 = Tag(TagType.Expense, "Petit déjeuner")
    var t2 = Tag(TagType.Account, "Paradis fiscal")
    transaction.addTag(t1)
    transaction.addTag(t2)
    val onClickReturn: () -> Unit = {
        Log.d(PLUTUS_DEBUG_TAG, "Touched return button")
    }
    TransactionDetailsView(transaction, onClickReturn)
}