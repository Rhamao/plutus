package com.example.plutus.utils.models

import android.os.Build
import android.util.Log.ERROR
import androidx.annotation.RequiresApi
import com.example.plutus.utils.Tag
import com.example.plutus.utils.TagType
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@RequiresApi(Build.VERSION_CODES.O)
class AccountBook(private var name: String) {
    private var currentAccount: Boolean = false
    private var transactions: ArrayList<Transaction> = ArrayList()
    private var amount: Float = 0F
    private var income: Float = 0F
    private var expense: Float = 0F
    private var date: String = LocalDate.now().toString()
    private var uniqueID = UUID.randomUUID().toString()
    private var tags: HashSet<Tag> = hashSetOf(
        Tag(TagType.Income, "Revenu"),
        Tag(TagType.Expense, "Dépense"),
        Tag(TagType.Account, "Compte principal")
    )

    fun setAccountBook(
        currentAccount: Boolean?,
        transactions: ArrayList<Transaction>?,
        amount: Float?,
        income: Float?,
        expense: Float?,
        date: String?,
        uniqueID: String?
    ): AccountBook {
        if (currentAccount != null)
            this.currentAccount = currentAccount
        if (transactions != null)
            this.transactions = transactions
        if (amount != null)
            this.amount = amount
        if (income != null)
            this.income = income
        if (expense != null)
            this.expense = expense
        if (date != null)
            this.date = date
        if (uniqueID != null)
            this.uniqueID = uniqueID
        //TODO GUILLAUME tags

        return this
    }

    fun generateDefault(): AccountBook {
        return AccountBook("Nouveau carnet")
    }

    fun duplicate(): AccountBook {
        val accountBook = AccountBook(name)
        accountBook.transactions.addAll(transactions)
        accountBook.amount = amount
        accountBook.income = income
        accountBook.expense = expense
        accountBook.tags.addAll(tags)
        return accountBook
    }

    fun getUniqueID(): String {
        return uniqueID
    }

    fun getDate(): String {
        return date
    }

    fun updateDate() {
        date = LocalDate.now().toString()
    }

    private fun setCurrentAccount(value: Boolean) {
        currentAccount = value
    }

    fun changeCurrentAccount(list: List<AccountBook>) {
        var change = false
        if (!isCurrent()) {
            for (account in list) {
                if (account.isCurrent()) {
                    account.setCurrentAccount(false)
                    change = true
                }
            }
            if (!change) {
                ERROR
                return
            }
            setCurrentAccount(true)
        }
    }

    fun modifyAccount(newName: String) {
        name = newName
    }

    fun isCurrent(): Boolean {
        return currentAccount
    }

    fun getName(): String {
        return name
    }

    fun getAmount(): Float {
        return amount
    }

    fun getIncome(): Float {
        return income
    }

    fun getExpense(): Float {
        return expense
    }

    fun getTransactions(): List<Transaction> {
        return transactions
    }

    fun addTransaction(transaction: Transaction) {
        transactions.add(transaction)
        when (transaction.getTransactionType()) {
            TagType.Income -> {
                income += transaction.amount
                amount += transaction.amount
            }
            TagType.Expense -> {
                expense += transaction.amount
                amount -= transaction.amount
            }
            else -> ERROR
        }
    }

    fun getTags(tagType: TagType): HashSet<Tag> {
        return tags
    }

    fun getAllTagsAsList(): List<Tag> {
        val list = arrayListOf<Tag>()
        list.addAll(tags.toList())
        return list
    }

    fun addTag(tag: Tag) {
        tags.add(tag)
    }

    fun resetTags(tags: HashSet<Tag>) {
        tags.removeAll(tags);
    }

    fun addTag(tagType: TagType, name: String) {
        tags.add(Tag(tagType, name))
    }

    fun removeTransaction(transaction: Transaction) {
        transactions.remove(transaction)
        when (transaction.getTransactionType()) {
            TagType.Income -> {
                income -= transaction.amount
                amount -= transaction.amount
            }
            TagType.Expense -> {
                expense -= transaction.amount
                amount += transaction.amount
            }
            else -> ERROR
        }
    }
}

fun intToBool(int: Int): Boolean {
    if (int > 0)
        return true
    return false
}

fun boolToInt(bool: Boolean): Int {
    if (bool)
        return 1
    return 0
}

@RequiresApi(Build.VERSION_CODES.O)
fun getCurrent(list: List<AccountBook>): AccountBook {
    var default: AccountBook = AccountBook("default");
    for (account in list) {
        if (account.isCurrent())
            default = account;
    }
    return default;
}