package com.example.plutus.views

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.plutus.utils.values.PaddingValue
import com.example.plutus.utils.values.TextSizeValue
import com.example.plutus.utils.*
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.TextInput
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AccountBookFormView(onClickCancel: () -> Unit, onClickValidate: (AccountBook) -> Unit) {
    var accountBook = AccountBook("Nouveau carnet")
    val nameUpdater: (String) -> Unit = { text: String -> accountBook.modifyAccount(text) }
    var textPadding = PaddingValue.SIMPLE.dp + 4.dp
    Column(
        Modifier
            .fillMaxHeight()
            .padding(PaddingValue.SIMPLE.dp)
    ) {
        Text(
            text = "Nouvelle transaction",
            fontSize = 26.sp,
            modifier = Modifier.padding(textPadding, 0.dp, 0.dp, 0.dp)
        )
        SimpleTextInput(
            textInput = TextInput("Titre", "Titre..."),
            PaddingValue.SIMPLE,
            nameUpdater
        )
        Row(Modifier.fillMaxWidth()) {
            Button(onClick = { onClickCancel() }) {
                Text(text = "Annuler", fontSize = TextSizeValue.BODY.sp)
            }
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                Button(onClick = { onClickValidate(accountBook) }) {
                    Text(text = "Valider", fontSize = TextSizeValue.BODY.sp)
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyAccountBookFormPreview() {
    var accountBook = AccountBook("Nouveau carnet")
    val cancel: () -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "CANCEL") }
    val validate: (AccountBook) -> Unit = { book: AccountBook ->
        accountBook = book
        Log.d(PLUTUS_DEBUG_TAG, "VALIDATE " + accountBook.getName())
    }
    AccountBookFormView(cancel, validate)
}