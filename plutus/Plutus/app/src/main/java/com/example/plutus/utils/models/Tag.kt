package com.example.plutus.utils

import androidx.compose.ui.graphics.Color
import com.example.plutus.utils.values.CustomColors
import java.util.*

enum class TagType(var color: Color, name: String) {
    Expense(CustomColors.ChalkRed, "Dépense"),
    Income(CustomColors.ChalkGreen, "Revenu"),
    Account(CustomColors.ChalkBlue, "Compte")
}

data class Tag(val type : TagType, val name : String) {
    private var uniqueID : String? = UUID.randomUUID().toString()

    private fun setID(ID: String) {
        uniqueID = ID;
    }

    fun getID() : String? {
        return uniqueID;
    }

    fun setTag(ID : String): Tag {
        this.setID(ID);
        return this;
    }
}

fun stringToTagType(type : String) : TagType {
    if(type == TagType.Expense.name)
        return TagType.Expense
    if(type == TagType.Income.name)
        return TagType.Income
    return TagType.Account;
}

fun tagTypeToString(tagtype : TagType) : String {
    if(tagtype == TagType.Income)
        return TagType.Income.name;
    if(tagtype == TagType.Expense)
        return TagType.Expense.name;
    return "Account";
}