package com.example.plutus.views

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.tooling.preview.Preview
import com.example.plutus.utils.*
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchView(
    accountBook: AccountBook,
    updateSelectedTransaction: (Transaction) -> Unit
) {
    var searchResult by remember { mutableStateOf(accountBook.getTransactions()) }
    var st = SearchTransactions(accountBook.getTransactions())
    val onExecuteSearch: (String) -> Unit = { searchResult = st.search(it) }

    Column {
        SearchAppBar(
            onExecuteSearch = onExecuteSearch,
        )
        //TODO change it so that amounts are calculated on the search result and not the accountBook
        //TransactionsViewAmountsBar(accountBook = accountBook)
        TransactionTilesColumn(
            items = searchResult,
            updateSelectedTransaction = updateSelectedTransaction,
            null
        )
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MySearchViewPreview() {
    val tags = hashSetOf(
        Tag(TagType.Expense, "Petit déjeuner"),
        Tag(TagType.Income, "Bonbons volés"),
        Tag(TagType.Account, "Paradis fiscal")
    )
    val transaction = Transaction(
        "Boulangerie",
        "Chocolatines x 5",
        LocalDate.parse("2022-05-12").toString(),
        tags,
        5.25F
    )
    val transaction2 = Transaction(
        "Babouche",
        "Chocolatines x 5",
        LocalDate.parse("2022-05-12").toString(),
        tags,
        5.25F
    )
    val accountBook = AccountBook("Voyage Japon")
    for (i in 1..3) {
        accountBook.addTransaction(transaction)
        accountBook.addTransaction(transaction2)
    }
    val foo: (Transaction) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "Touched " + it.name) }

    SearchView(accountBook = accountBook, updateSelectedTransaction = foo)

}