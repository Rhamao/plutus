package com.example.plutus

import android.content.Context
import android.os.Build
import android.util.Log
import android.widget.Toast
import android.widget.Toast.makeText
import androidx.annotation.RequiresApi
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.plutus.database.AccountDB
import com.example.plutus.database.TagDB
import com.example.plutus.database.TransactionDB
import com.example.plutus.utils.Tag
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.models.getCurrent
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import com.example.plutus.views.*


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Navigation(navController: NavHostController) {
    val context = LocalContext.current
    val accountDB = AccountDB(LocalContext.current);
    val transactionDB = TransactionDB(LocalContext.current);
    val tagDB = TagDB(LocalContext.current);

    if (accountDB.getAccountBooks(LocalContext.current).size == 0) {
        val defaultAccountBook = AccountBook("Carnet courant");
        defaultAccountBook.setAccountBook(true, null, null, null, null, null, null)
        accountDB.addAccountBook(defaultAccountBook);
    }
    val listAccountBook: ArrayList<AccountBook> = accountDB.getAccountBooks(LocalContext.current);

    println(listAccountBook.size)

    var selectedAccountBook by remember { mutableStateOf(getCurrent(listAccountBook)) }
    LaunchedEffect(selectedAccountBook) {
        val list = tagDB.getTags("null");
        selectedAccountBook.resetTags(list)
        for (tag in list) {
            selectedAccountBook.addTag(tag)
        }
    }
    var accountBooks by remember { mutableStateOf(listAccountBook.toList()) }
    var selectedTransaction by remember { mutableStateOf(Transaction.getDefault()) }

    NavHost(navController = navController, startDestination = NavigationRoutes.ACCOUNT_BOOKS) {
        composable(NavigationRoutes.ACCOUNT_BOOKS) {
            showBottomBar()
            showFloatingButton()
            val updateSelectedAccountBook: (AccountBook) -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Select account book " + it.getName())
                selectedAccountBook = it
            }
            val onClickDelete: (AccountBook) -> Unit = { accountBook ->
                Log.d(PLUTUS_DEBUG_TAG, "Clicked delete account book " + accountBook.getName())
                if (accountBooks.size > 1) {
                    accountBooks = accountBooks.toMutableList().also { it.remove(accountBook) }
                    if (selectedAccountBook.equals(accountBook)) {
                        selectedAccountBook = accountBooks.get(0)
                    }
                } else {
                    Toast
                        .makeText(
                            context,
                            "Vous ne pouvez pas posséder moins de 1 carnet de compte.",
                            Toast.LENGTH_LONG
                        )
                        .show()
                }
            }
            AccountBooksView(
                items = accountBooks,
                updateSelectedAccountBook,
                selectedAccountBook.getUniqueID(),
                onClickDelete
            )
        }

        composable(NavigationRoutes.TRANSACTIONS) {
            showBottomBar()
            showFloatingButton()
            val unit: () -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "TOUCHED") }
            val foo: (Transaction) -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched transaction " + it)
                selectedTransaction = it
                navController.navigate(NavigationRoutes.TRANSACTION)
            }
            val onClickDelete: (Transaction) -> Unit = { transaction ->
                Log.d(PLUTUS_DEBUG_TAG, "Clicked delete transaction " + transaction.name)
                selectedAccountBook.removeTransaction(transaction = transaction)
                navController.navigate(NavigationRoutes.TRANSACTIONS)
            }
            TransactionsView(
                accountBook = selectedAccountBook,
                updateSelectedTransaction = foo,
                threeDotsOnClick = unit,
                onClickOnDelete = onClickDelete
            )
        }
        composable(NavigationRoutes.SEARCH) {
            showBottomBar()
            hideFloatingButton()
            val foo: (Transaction) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "Touched " + it.name) }
            SearchView(accountBook = selectedAccountBook, updateSelectedTransaction = foo)
        }
        composable(NavigationRoutes.ACCOUNT_BOOK_FORM) {
            hideBottomBar()
            hideFloatingButton()
            val cancel: () -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched cancel button")
                navController.navigate(NavigationRoutes.ACCOUNT_BOOKS)
            }
            val validate: (AccountBook) -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched validate button")
                accountDB.changeCurrent(context)
                accountDB.addAccountBook(it);
                //accountBooks.add(it)
                accountBooks = accountBooks + listOf(it)
                selectedAccountBook = it
                navController.navigate(NavigationRoutes.TRANSACTIONS)
            }
            AccountBookFormView(onClickCancel = cancel, onClickValidate = validate)
        }
        composable(NavigationRoutes.TRANSACTION_FORM) {
            hideBottomBar()
            hideFloatingButton()
            val cancel: () -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched cancel button")
                navController.navigate(NavigationRoutes.TRANSACTIONS)
            }
            val validate: (Transaction) -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched validate button")
                //TODO GUILLAUME add new transaction to DB and to local lists
                transactionDB.addTransaction(it, selectedAccountBook.getUniqueID());
                selectedAccountBook.addTransaction(it)
                navController.navigate(NavigationRoutes.TRANSACTIONS)
            }
            val tagsAdder: (Tag) -> Unit = {
                Log.d(
                    PLUTUS_DEBUG_TAG,
                    "Added tag " + it + " to account book " + selectedAccountBook
                )
                tagDB.addTag(it.getID().toString(), it.type.toString(), "null", it.name);
                selectedAccountBook.addTag(it)
            }
            TransactionFormView(
                onClickCancel = cancel,
                onClickValidate = validate,
                tagsAdder = tagsAdder,
                existingTags = selectedAccountBook.getAllTagsAsList(),
                null
            )
        }
        composable(NavigationRoutes.TRANSACTION) {
            hideBottomBar()
            hideFloatingButton()
            val onClickReturn: () -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Touched return button")
                navController.navigate(NavigationRoutes.TRANSACTIONS)
            }
            TransactionDetailsView(selectedTransaction, onClickReturn)
        }
    }
}

@Preview
@Composable
fun MyPreview() {

    Scaffold(floatingActionButton = {
        FloatingActionButton(
            onClick = {},
            // We specify the same shape that we passed as the cutoutShape above.
            shape = RoundedCornerShape(50),
            // We use the secondary color from the current theme. It uses the defaults when
            // you don't specify a theme (this example doesn't specify a theme either hence
            // it will just use defaults. Look at DarkModeActivity if you want to see an
            // example of using themes.
            backgroundColor = MaterialTheme.colors.secondary
        ) {
            IconButton(onClick = {}) {
                Icon(imageVector = Icons.Filled.Add, contentDescription = "Favorite")
            }
        }
    }) {
        //WalletScreen();
    }
}