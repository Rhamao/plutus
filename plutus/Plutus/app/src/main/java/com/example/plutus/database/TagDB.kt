package com.example.plutus.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.plutus.database.Helper.Helper
import com.example.plutus.utils.Tag
import com.example.plutus.utils.stringToTagType
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG

class TagDB(context : Context) {
    private val database: SQLiteDatabase by lazy {
        val helper = Helper(context)
        helper.writableDatabase
    }

    fun getTags(transactionID : String) : HashSet<Tag> {
        val tags = HashSet<Tag>()
        val query = "SELECT * FROM Tag WHERE TransactionID = \"$transactionID\";"
        val cursor = database.rawQuery(query, null);

        while(cursor.moveToNext()) {
            val tag = Tag(stringToTagType(cursor.getString(1)), cursor.getString(2))
            tags.add(tag.setTag(cursor.getString(0)))
            Log.d(PLUTUS_DEBUG_TAG, tag.name)
        }
        cursor.close();

        return tags;
    }

    fun deleteTag(ID : String): Boolean {
        val cursor = database.delete("Tag", "TagID = $ID", null)
        if (cursor > 0)
            return true;
        return false
    }

    fun addTag(tagID : String, tagType : String, transactionID: String, name : String): Boolean {
        val values = ContentValues();
        values.put("TagID", tagID);
        values.put("TagType", tagType);
        values.put("Name", name);
        values.put("TransactionID", transactionID)
        val cursor = database.insert("Tag", null, values);

        if(cursor == -1L)
            return false;
        return true;
    }
}