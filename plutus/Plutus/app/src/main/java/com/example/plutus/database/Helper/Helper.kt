package com.example.plutus.database.Helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.ui.platform.LocalContext
import com.example.plutus.database.AccountDB
import com.example.plutus.utils.models.AccountBook
import java.time.LocalDate
import java.util.*

class Helper(
    context: Context,
) : SQLiteOpenHelper(context, DATABASE_NAME, null, CURRENT_VERSION) {

    companion object {
        const val DATABASE_NAME = "Plutus"
        const val CURRENT_VERSION = 1

        const val ACCOUNTBOOK_TABLE = """CREATE TABLE AccountTable (
            AccountBookID TEXT,
            Name TEXT,
            CurrentAccount INTEGER,
            Amount INTEGER,
            Income INTEGER,
            Expense INTEGER,
            Date TEXT,
            PRIMARY KEY (AccountBookID)
            )"""

        const val TRANSACTION_TABLE = """CREATE TABLE TransactionTable (
            TransactionID TEXT,
            Name TEXT,
            Text TEXT,
            Date TEXT,
            Amount INTEGER,
            AccountBookID TEXT,
            PRIMARY KEY (TransactionID),
            FOREIGN KEY (AccountBookID)
                REFERENCES AccountBook (AccountBookID)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE
            )"""

        const val TAG_TABLE = """CREATE TABLE Tag (
            TagID TEXT,
            TagType TEXT,
            Name TEXT,
            TransactionID TEXT,
            PRIMARY KEY (TagID, TransactionID),
            FOREIGN KEY (TransactionID)
                REFERENCES TransactionTable (TransactionID)
                    ON DELETE CASCADE
                    ON UPDATE CASCADE
            )"""
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(ACCOUNTBOOK_TABLE)
        db.execSQL(TRANSACTION_TABLE)
        db.execSQL(TAG_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // to be implemented if the schema changes from oldVersion to newVersion
        // we use ALTER TABLE SQL statements to transform the tables
    }
}