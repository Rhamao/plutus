package com.example.plutus.utils.values

class TextSizeValue {
    companion object {
        const val SUB_TITLE_1 = 20
        const val BODY = 12
    }
}