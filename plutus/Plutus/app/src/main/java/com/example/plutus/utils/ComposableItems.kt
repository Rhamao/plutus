package com.example.plutus.utils

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.BottomNav
import com.example.plutus.utils.models.TextInput
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.CustomColors
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import com.example.plutus.utils.values.TextSizeValue
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BottomNavigationBar(
    items: List<BottomNav>,
    navController: NavController,
    modifier: Modifier = Modifier,
    onItemClick: (BottomNav) -> Unit
) {
    val backStackEntry = navController.currentBackStackEntryAsState()
    BottomNavigation(
        modifier = modifier,
        backgroundColor = Color.DarkGray,
        elevation = 5.dp
    ) {
        items.forEach { item ->
            val selected = item.route == backStackEntry.value?.destination?.route
            BottomNavigationItem(
                selected = selected,
                onClick = { onItemClick(item) },
                selectedContentColor = CustomColors.ChalkGreen,
                unselectedContentColor = Color.Gray,
                icon = {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = item.name
                        )
                        if (selected) {
                            Text(
                                text = item.name,
                                textAlign = TextAlign.Center,
                                fontSize = 10.sp
                            )
                        }
                    }
                }
            )
        }
    }
}

@Composable
fun SimpleTextInput(textInput: TextInput, padding: Int, stringUpdater: (String) -> Unit) {
    var textValue by remember { mutableStateOf(TextFieldValue("")) }
    TextField(
        value = textValue,
        onValueChange = {
            textValue = it
            if (textValue.text.isNotEmpty()) {
                stringUpdater(textValue.text)
            }
        },
        label = { Text(textInput.label) },
        placeholder = { Text(text = textInput.placeholder) },
        modifier = Modifier
            .padding(padding.dp)
            .fillMaxWidth()
            .background(CustomColors.LighterGray)
    )
}

@Composable
fun TagInput(
    textInput: TextInput,
    padding: Int,
    stringUpdater: (String) -> Unit,
    width: Int,
    height: Int
) {
    var textValue by remember { mutableStateOf(TextFieldValue("")) }
    TextField(
        value = textValue,
        onValueChange = {
            textValue = it
            if (textValue.text.isNotEmpty()) {
                stringUpdater(textValue.text)
            }
        },
        label = { Text(textInput.label) },
        placeholder = { Text(text = textInput.placeholder) },
        modifier = Modifier
            .padding(padding.dp)
            .width(width.dp)
            .height(height.dp)
            .background(CustomColors.LighterGray)
    )
}

@Composable
fun StringSelector(
    textInput: TextInput,
    padding: Int,
    stringUpdater: (String) -> Unit,
    strings: List<String>,
    width: Int,
    height: Int
) {
    var textValue by remember { mutableStateOf(TextFieldValue("")) }
    var unfoldTagList by remember { mutableStateOf(false) }
    val textFieldUpdater: (String) -> Unit = {
        textValue = TextFieldValue(it)
        stringUpdater(it)
        unfoldTagList = !unfoldTagList
    }
    val onClick: () -> Unit = {
        Log.d(PLUTUS_DEBUG_TAG, "Unfold")
        unfoldTagList = !unfoldTagList
    }
    Column() {
        TextField(
            value = textValue,
            onValueChange = {
                textValue = it
                if (textValue.text.isNotEmpty()) {
                    textFieldUpdater(textValue.text)
                }
            },
            label = { Text(textInput.label) },
            placeholder = { Text(text = textInput.placeholder) },
            modifier = Modifier
                .padding(padding.dp)
                .width(width.dp)
                .height(height.dp)
                .background(CustomColors.LighterGray)
                .clickable { onClick() },
            enabled = false
        )
        stringUnfoldableList(
            unfoldTagList,
            strings,
            stringUpdater = textFieldUpdater,
            width,
            height / 2,
            padding
        )
    }
}

@Composable
fun stringUnfoldableList(
    unfoldTagList: Boolean,
    strings: List<String>,
    stringUpdater: (String) -> Unit,
    width: Int,
    height: Int,
    padding: Int
) {
    if (unfoldTagList) {
        LazyColumn() {
            items(items = strings, itemContent = {
                Card(
                    shape = RoundedCornerShape(4.dp),
                    backgroundColor = CustomColors.LighterGray,
                    modifier = Modifier
                        .padding(padding.dp, 2.dp, 0.dp, 0.dp)
                        .width(width = width.dp)
                        .height(height = height.dp)
                        .clickable { stringUpdater(it) }
                ) {
                    Text(
                        text = it,
                        fontSize = TextSizeValue.BODY.sp,
                        textAlign = TextAlign.Center
                    )
                }
            })
        }
    }
}

@Composable
fun NumberTextInput(floatUpdater: (Float) -> Unit, padding: Int) {
    var textValue by remember { mutableStateOf(TextFieldValue("00.00")) }
    TextField(
        value = textValue,
        modifier = Modifier
            .padding(padding.dp)
            .fillMaxWidth()
            .background(CustomColors.LighterGray),
        onValueChange = {
            if (it.text.isNotEmpty()) {
                textValue = it
                floatUpdater(textValue.text.toString().toFloat())
            }
        },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Number
        )
    )
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun DateInput(padding: Int, dateUpdater: (LocalDate) -> Unit) {
    val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val date = LocalDate.now()
    var textValue by remember { mutableStateOf(TextFieldValue(date.toString())) }
    TextField(
        value = textValue,
        onValueChange = {
            if (it.text.isNotEmpty()) {
                textValue = it
                if (isValidDate(textValue.text)) {
                    val parsedDate = LocalDate.parse(textValue.text, dateFormat)
                    dateUpdater(parsedDate)
                }
            }

        },
        label = { Text("Date") },
        placeholder = { Text(text = date.toString()) },
        modifier = Modifier
            .padding(padding.dp)
            .fillMaxWidth()
            .background(CustomColors.LighterGray)
    )
}

@RequiresApi(Build.VERSION_CODES.O)
private fun isValidDate(dateStr: String?): Boolean {
    val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    try {
        dateFormat.parse(dateStr)
    } catch (e: DateTimeParseException) {
        return false
    }
    return true
}

@Composable
fun TagTile(tag: Tag, padding: Int) {
    Box(
        Modifier
            .padding(padding.dp)
            .background(
                color = tag.type.color,
                shape = RoundedCornerShape(5.dp)
            )
    ) {
        Text(text = tag.name, Modifier.padding(2.dp))
    }
}

var smallIconSize = 17.dp

@Composable
fun TransactionTile(transaction: Transaction, onClickOnDelete: ((Transaction) -> Unit)?) {

    Row(Modifier.fillMaxWidth()) {
        Column() {
            Text(text = transaction.name, fontSize = TextSizeValue.SUB_TITLE_1.sp)
            LazyRow(Modifier.padding(2.dp)) {
                items(items = transaction.tags.toList(), itemContent = { tag ->
                    TagTile(tag = tag, 2)
                })
            }
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
            Column(horizontalAlignment = Alignment.End) {
                if (onClickOnDelete != null) {
                    Icon(
                        Icons.Filled.Delete,
                        contentDescription = "Amount",
                        Modifier
                            .size(smallIconSize)
                            .clickable {
                                onClickOnDelete(transaction)
                            },
                        tint = Color.Black
                    )
                }
                Text(text = transaction.amount.toString() + " €", fontSize = TextSizeValue.BODY.sp)
                Text(text = transaction.date.toString(), fontSize = TextSizeValue.BODY.sp)
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AccountBookTile(
    accountBook: AccountBook,
    isSelected: Boolean,
    updateSelectedAccountBook: (AccountBook) -> Unit,
    onClickOnDelete: (AccountBook) -> Unit
) {
    Row(
        Modifier
            .fillMaxWidth()
            .padding(2.dp)
            .height(IntrinsicSize.Max)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.fillMaxHeight()) {
            Icon(
                Icons.Filled.AccountBalanceWallet,
                contentDescription = "Account book icon",
                Modifier.size(60.dp),
            )
        }

        Column() {
            Text(text = accountBook.getName(), fontSize = TextSizeValue.SUB_TITLE_1.sp)
            Row(verticalAlignment = Alignment.CenterVertically) {
                if (isSelected) {
                    Icon(
                        Icons.Filled.CheckCircle,
                        contentDescription = "Checkbox",
                        Modifier.size(smallIconSize),
                        tint = CustomColors.ChalkGreen
                    )
                    Text(
                        text = "Carnet de compte courant",
                        fontSize = TextSizeValue.BODY.sp,
                        color = CustomColors.ChalkGreen
                    )
                } else {
                    Icon(
                        Icons.Filled.Circle,
                        contentDescription = "Checkbox",
                        Modifier
                            .size(smallIconSize)
                            .clickable { updateSelectedAccountBook(accountBook) },
                        tint = Color.Gray
                    )
                }
            }
            Text(
                text = "Date de création : " + accountBook.getDate(),
                fontSize = TextSizeValue.BODY.sp,
                color = Color.Gray
            )
        }
        Column(verticalArrangement = Arrangement.Bottom, modifier = Modifier.fillMaxHeight()) {
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.End
            ) {
                Icon(
                    Icons.Filled.Delete,
                    contentDescription = "Amount",
                    Modifier
                        .size(smallIconSize)
                        .clickable { onClickOnDelete(accountBook) },
                    tint = Color.Black
                )
            }
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = accountBook.getAmount().toString(),
                    fontSize = TextSizeValue.BODY.sp,
                    modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
                )
                Icon(
                    Icons.Filled.Savings,
                    contentDescription = "Amount",
                    Modifier.size(smallIconSize),
                    tint = CustomColors.ChalkBlue
                )
            }
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = accountBook.getExpense().toString(),
                    fontSize = TextSizeValue.BODY.sp,
                    modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
                )
                Icon(
                    Icons.Filled.RemoveCircle,
                    contentDescription = "Expense",
                    Modifier.size(smallIconSize),
                    tint = CustomColors.ChalkRed
                )
            }
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.End,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = accountBook.getIncome().toString(),
                    fontSize = TextSizeValue.BODY.sp,
                    modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
                )
                Icon(
                    Icons.Filled.AddCircle,
                    contentDescription = "Income",
                    Modifier.size(smallIconSize),
                    tint = CustomColors.ChalkGreen
                )
            }
        }
    }
}

@Composable
fun TransactionTilesColumn(
    items: List<Transaction>,
    updateSelectedTransaction: (Transaction) -> Unit,
    onClickOnDelete: ((Transaction) -> Unit)?
) {
    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        items(items = items, itemContent = { item ->
            val index = items.indexOf(item)
            Row(modifier = Modifier.fillParentMaxWidth()) {
                Card(
                    shape = RoundedCornerShape(4.dp),
                    modifier = Modifier
                        .fillParentMaxWidth()
                        .padding(5.dp)
                        .clickable { updateSelectedTransaction(item) },
                    backgroundColor = CustomColors.LighterGray
                ) {
                    TransactionTile(transaction = item, onClickOnDelete = onClickOnDelete)
                }
            }
        })
    }
}

@Composable
fun IdFilteredTransactionTilesColumn(
    filteredIds: List<String>,
    idsToTransactions: HashMap<String, Transaction>,
    onClickItems: List<() -> Unit>
) {
    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        items(items = filteredIds, itemContent = { id ->
            val index = filteredIds.indexOf(id)
            Row(modifier = Modifier.fillParentMaxWidth()) {
                Card(
                    shape = RoundedCornerShape(4.dp),
                    modifier = Modifier
                        .fillParentMaxWidth()
                        .padding(5.dp)
                        .clickable { onClickItems[index]() }
                ) {
                    idsToTransactions.get(id)?.let { TransactionTile(transaction = it, onClickOnDelete = null) }
                }
            }
        })
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AccountBooksColumn(
    items: List<AccountBook>,
    updateSelectedAccountBook: (AccountBook) -> Unit,
    selectedAccountBookId: String,
    onClickOnDelete: (AccountBook) -> Unit
) {
    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        items(items = items, itemContent = { item ->
            val index = items.indexOf(item)
            Row(modifier = Modifier.fillParentMaxWidth()) {
                Card(
                    shape = RoundedCornerShape(4.dp),
                    modifier = Modifier
                        .fillParentMaxWidth()
                        .padding(5.dp),
                    backgroundColor = CustomColors.LighterGray
                ) {
                    AccountBookTile(
                        accountBook = item,
                        isSelected = item.getUniqueID().equals(selectedAccountBookId),
                        updateSelectedAccountBook = updateSelectedAccountBook,
                        onClickOnDelete = onClickOnDelete
                    )
                }
            }
        })
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TransactionsViewAmountsBar(accountBook: AccountBook) {
    var smallIconSize = 17.dp
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = String.format("%.2f", accountBook.getAmount()),
            fontSize = TextSizeValue.BODY.sp,
            modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
        )
        Icon(
            Icons.Filled.Savings,
            contentDescription = "Amount",
            Modifier.size(smallIconSize),
            tint = CustomColors.ChalkBlue
        )
        Text(
            text = String.format("%.2f", accountBook.getExpense()),
            fontSize = TextSizeValue.BODY.sp,
            modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
        )
        Icon(
            Icons.Filled.RemoveCircle,
            contentDescription = "Expense",
            Modifier.size(smallIconSize),
            tint = CustomColors.ChalkRed
        )
        Text(
            text = String.format("%.2f", accountBook.getIncome()),
            fontSize = TextSizeValue.BODY.sp,
            modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
        )
        Icon(
            Icons.Filled.AddCircle,
            contentDescription = "Income",
            Modifier.size(smallIconSize),
            tint = CustomColors.ChalkGreen
        )
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TransactionsViewTopBar(accountBookName: String, onClick: () -> Unit) {
    var smallIconSize = 17.dp
    Row(
        Modifier
            .fillMaxWidth()
            .padding(2.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = accountBookName,
            fontSize = TextSizeValue.SUB_TITLE_1.sp,
            modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
        )
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End
        ) {
            Icon(
                Icons.Filled.MoreVert,
                contentDescription = "Income",
                modifier = Modifier.clickable { onClick() }
            )
        }
    }
}

@Composable
fun MoneyGoalSlider(min: Float, max: Float, actualValue: Float, moneySign: String) {
    var sliderValue by remember { mutableStateOf(actualValue / (max - min)) }
    val textWeight = 0.15F
    val sliderWeight = 0.7F
    Column() {
        Row(
            Modifier
                .fillMaxWidth(), verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                String.format("%.2f", min) + moneySign,
                Modifier.weight(textWeight),
                textAlign = TextAlign.End
            )

            Slider(
                value = sliderValue,
                modifier = Modifier
                    .weight(sliderWeight),
                onValueChange = { newValue ->
                    sliderValue = newValue
                },
                steps = 3,
                colors = SliderDefaults.colors(
                    activeTickColor = Color.Transparent,
                    inactiveTickColor = Color.DarkGray,
                    inactiveTrackColor = Color.LightGray,
                    activeTrackColor = CustomColors.ChalkBlue,
                    thumbColor = CustomColors.ChalkBlue
                )
            )
            Text(String.format("%.2f", max) + moneySign, Modifier.weight(textWeight))
        }
        Row() {
            Box(Modifier.weight(textWeight + sliderValue * sliderWeight))
            if (sliderValue != 0F && sliderValue != 1.0F) {
                Text(
                    text = String.format("%.2f", sliderValue * (max - min) + min),
                    modifier = Modifier.weight(sliderWeight - sliderWeight * sliderValue + textWeight)
                )
            }
        }
    }
}

//TODO replace all onClick list by hashmap<id, onClickCallback>

@RequiresApi(Build.VERSION_CODES.O)
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchAppBar(
    onExecuteSearch: (String) -> Unit,
) {
    var query by remember { mutableStateOf("") }
    val keyboardController = LocalSoftwareKeyboardController.current

    Row(modifier = Modifier.fillMaxWidth()) {
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            value = query,
            onValueChange = {
                query = it
            },
            label = { Text(text = "Chercher ici...") },
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done,
            ),
            keyboardActions = KeyboardActions(
                onDone = {
                    onExecuteSearch(query)
                    keyboardController?.hide()
                },
            ),
            leadingIcon = { Icon(Icons.Filled.Search, contentDescription = "Search Icon") },
            textStyle = TextStyle(color = Color.Black),
            colors = TextFieldDefaults.textFieldColors(backgroundColor = CustomColors.LighterGray),
        )
    }

}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyComposableItemsPreview() {
    val unit: () -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "TOUCHED") }
    val tagList = hashSetOf(
        Tag(TagType.Expense, "Petit déjeuner"),
        Tag(TagType.Income, "Bonbons volés"),
        Tag(TagType.Account, "Paradis fiscal")
    )

    val transaction = Transaction(
        "Boulangerie",
        "Chocolatines x 5",
        LocalDate.parse("2022-05-12").toString(),
        tagList,
        5.25F
    )

    val transaction2 = Transaction(
        "Babouche",
        "Chocolatines x 5",
        LocalDate.parse("2022-05-12").toString(),
        tagList,
        5.25F
    )

    val accountBook = AccountBook("Voyage Japon")
    for (i in 1..3) {
        accountBook.addTransaction(transaction)
        accountBook.addTransaction(transaction2)
    }

    var list = arrayListOf<Transaction>()
    var listoc = arrayListOf<() -> Unit>()

    for (i in 1..3) {
        list.add(transaction)
        listoc.add(unit)
    }

    for (i in 1..3) {
        list.add(transaction2)
        listoc.add(unit)
    }

    /*Column() {
        val mContext = LocalContext.current
        AccountBookTile(accountBook = accountBook, isSelected = false)
        AccountBookTile(accountBook = accountBook, isSelected = true)
    }*/
    /*TransactionTilesColumn(
        items = list, onClickItems = listoc
    )*/
    //TransactionsViewAmountsBar(accountBook = accountBook)
    //TransactionsViewTopBar(accountBook.getName(), unit)
    //MoneyGoalSlider(0.7F, 326.13F, 30F, "€")

    var st = SearchTransactions(list)
    val onQueryChanged: (String) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "onQueryChanged") }
    val onExecuteSearch: (String) -> List<Transaction> = { st.search(it) }
    val foo: (Transaction) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "Touched " + it.name) }

    /*SearchAppBar(
        onExecuteSearch,
        accountBook,
        foo,
    )*/

}
