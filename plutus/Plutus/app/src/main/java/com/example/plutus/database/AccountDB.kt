package com.example.plutus.database

import android.accounts.Account
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.os.Build
import android.util.Log
import android.util.Log.INFO
import android.util.Log.VERBOSE
import androidx.annotation.RequiresApi
import com.example.plutus.database.Helper.Helper
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.models.boolToInt
import com.example.plutus.utils.models.intToBool

class AccountDB(context : Context) {
    private val database: SQLiteDatabase by lazy {
        val helper = Helper(context)
        helper.writableDatabase
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun getAccountBooks(context: Context) : ArrayList<AccountBook> {
        val accountBooks = ArrayList<AccountBook>()
        val query = "SELECT * FROM AccountTable;"
        val cursor = database.rawQuery(query, null);
        val transactionDB = TransactionDB(context = context);

        while (cursor.moveToNext()) {
            val accountBook = AccountBook(
                cursor.getString(1)
            ).setAccountBook(
                intToBool(cursor.getInt(2)),
                transactionDB.getTransactions(cursor.getString(0), context = context),
                cursor.getFloat(3),
                cursor.getFloat(4),
                cursor.getFloat(5),
                cursor.getString(6),
                cursor.getString(0)
            )
            accountBooks.add(accountBook);
        }
        return accountBooks;
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getAccountBook(ID : String, context: Context) : AccountBook {
        val query = "SELECT * FROM AccountTable WHERE AccountBookID = \"$ID\";"
        val cursor = database.rawQuery(query, null)
        val transactionDB = TransactionDB(context = context);

        val id = cursor.getString(0);

        return AccountBook(
            cursor.getString(1)
        ).setAccountBook(
            intToBool(cursor.getInt(2)),
            transactionDB.getTransactions(cursor.getString(0), context = context),
            cursor.getFloat(3),
            cursor.getFloat(4),
            cursor.getFloat(5),
            cursor.getString(6),
            id
        )
    }

    fun deleteAccountBook(ID : String) : Boolean {
        val cursor = database.delete("AccountTable", "AccountBookID = $ID", null)
        if (cursor > 0)
            return true;
        return false
    }

    @SuppressLint("Recycle")
    @RequiresApi(Build.VERSION_CODES.O)
    fun changeCurrent(context: Context) {
        val list = getAccountBooks(context = context);
        var currentAccountBook: AccountBook? = null;
        for (count in list) {
            if (count.isCurrent()) {
                currentAccountBook = count;
            }
        }
        if (currentAccountBook != null) {
            val currentID = currentAccountBook.getUniqueID();
            var query =
                "UPDATE AccountTable SET CurrentAccount = 0 WHERE AccountBookID = \"$currentID\";"
            database.rawQuery(query, null);
        }
    }



    @RequiresApi(Build.VERSION_CODES.O)
    fun addAccountBook(accountBook : AccountBook) : Boolean {
        val values = ContentValues();
        values.put("AccountBookID", accountBook.getUniqueID());
        values.put("Name", accountBook.getName());
        values.put("CurrentAccount", boolToInt(accountBook.isCurrent()));
        values.put("Amount", accountBook.getAmount());
        values.put("Income", accountBook.getIncome());
        values.put("Expense", accountBook.getExpense());
        values.put("Date", accountBook.getDate());

        val cursor = database.insert("AccountTable", null, values);

        if(cursor == -1L)
            return false;
        return true;
    }
}