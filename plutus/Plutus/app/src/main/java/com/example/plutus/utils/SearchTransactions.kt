package com.example.plutus.utils

import android.util.Log
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG

class SearchTransactions(var transactions: List<Transaction>) {

    fun search(query: String):List<Transaction> {
        Log.d(PLUTUS_DEBUG_TAG, "New search query " + query)
        var newList = arrayListOf<Transaction>()

        transactions.forEach { transaction ->
            if(transaction.name.contains(query, true) || transaction.tags.any { tag -> tag.name.contains(query, true) }){
                Log.d(PLUTUS_DEBUG_TAG, "Found transaction for query " + query + " : " + transaction.name)
                newList.add(transaction)
            }
        }
        return newList.toList()
    }

}