package com.example.plutus.views

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.plutus.utils.*
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TransactionsView(
    accountBook: AccountBook,
    updateSelectedTransaction: (Transaction) -> Unit,
    threeDotsOnClick: () -> Unit,
    onClickOnDelete: ((Transaction) -> Unit)?
) {
    Column(modifier = Modifier.fillMaxHeight()) {
        TransactionsViewTopBar(accountBookName = accountBook.getName(), onClick = threeDotsOnClick)
        TransactionsViewAmountsBar(accountBook = accountBook)
        TransactionTilesColumn(items = accountBook.getTransactions(), updateSelectedTransaction = updateSelectedTransaction, onClickOnDelete = onClickOnDelete)
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyTransactionsViewPreview() {
    val accountBook = AccountBook("Voyage Japon")
    var list = arrayListOf<Transaction>()
    val foo: (Transaction) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "TOUCHED " + it.name) }
    val foo3dots: () -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "TOUCHED 3 dots") }
    val tags = hashSetOf(
        Tag(TagType.Expense, "Petit déjeuner"),
        Tag(TagType.Income, "Bonbons volés"),
        Tag(TagType.Account, "Paradis fiscal")
    )
    val transaction = Transaction(
        "Boulangerie",
        "Chocolatines x 5",
        LocalDate.parse("2022-05-12").toString(),
        tags,
        5.25F
    )
    for (i in 1..20){
        accountBook.addTransaction(transaction)
    }

    TransactionsView(accountBook = accountBook, updateSelectedTransaction = foo, threeDotsOnClick = foo3dots, null)
}