package com.example.plutus.utils.models

import android.os.Build
import androidx.annotation.RequiresApi
import com.example.plutus.utils.Tag
import com.example.plutus.utils.TagType
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class Transaction(var name : String, var text : String, var date : String, val tags : HashSet<Tag>, var amount : Float) {
    private var uniqueID = UUID.randomUUID().toString()

    fun setTransaction(ID : String) : Transaction {
        this.setUniqueID(ID);
        return this;
    }

    companion object {
        @RequiresApi(Build.VERSION_CODES.O)
        fun getDefault(): Transaction{
            return Transaction(
                "Nouvelle transaction",
                "Description",
                LocalDate.now().toString(),
                hashSetOf(),
                0F
            ).duplicate()
        }
    }

    fun duplicate(): Transaction {
        return Transaction(name, text, date, tags, amount)
    }

    fun getID() : String {
        return this.uniqueID
    }

    private fun setUniqueID(ID : String) {
        this.uniqueID = ID
    }
    /**
     * Return false if the transaction is an expense, true if it's an income
     */
    fun getTransactionType(): TagType? {

        /*for (tag in tags) {
            if (tag.type == TagType.Expense)
                return false
            if (tag.type == TagType.Income)
                return true
        }*/
        return tags.find { tag -> !tag.type.equals(TagType.Account) }?.type
    }

    fun modifyTransaction(name : String?, text : String?, date : String?, amount : Float?) {
        if(amount != null) {
            this.amount = amount
        }
        if (name != null) {
            this.name = name
        }
        if (text != null) {
            this.text = text
        }
        if (date != null) {
            this.date = date
        }
    }

    fun addTag(tag: Tag) {
        if (tag !in this.tags) {
            tags.add(tag)
        }
    }

    fun addTags(tags: List<Tag>) {
        tags.forEach { addTag(it) }
    }

    fun removeTag(tag: Tag) {
        if (tag in this.tags) {
            tags.remove(tag)
        }
    }

    fun checkType() {

    }
}