package com.example.plutus.utils.models

import androidx.compose.ui.graphics.vector.ImageVector

data class BottomNav(
    val name: String,
    val route: String,
    val icon: ImageVector
)