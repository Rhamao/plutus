package com.example.plutus

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.rememberNavController
import com.example.plutus.database.AccountDB
import com.example.plutus.database.TagDB
import com.example.plutus.database.TransactionDB
import com.example.plutus.ui.theme.PlutusTheme
import com.example.plutus.utils.models.BottomNav
import com.example.plutus.utils.BottomNavigationBar
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG

class MainActivity : ComponentActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContent {
            PlutusTheme {
                MyScaffold()
            }
        }
    }
}

var enableBottomBar = mutableStateOf(true)
var enableFloatingButton = mutableStateOf(true)
var floatingButtonAlpha = mutableStateOf(1F)

fun hideBottomBar(){
    enableBottomBar.value = false
}

fun showBottomBar(){
    enableBottomBar.value = true
}

fun hideFloatingButton(){
    enableFloatingButton.value = false
    floatingButtonAlpha.value = 0F
}

fun showFloatingButton(){
    enableFloatingButton.value = true
    floatingButtonAlpha.value = 1F
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MyScaffold() {
    val accountDB = AccountDB(LocalContext.current);
    val transactionDB = TransactionDB(LocalContext.current);
    val tagDB = TagDB(LocalContext.current);
    val enabledBB by remember { enableBottomBar }
    val enabledFB by remember { enableFloatingButton }
    var fbAlpha by remember { floatingButtonAlpha }
    val coroutineScope = rememberCoroutineScope()
    val navController = rememberNavController()
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    Scaffold(
        scaffoldState = scaffoldState,
        bottomBar = {
            if(enabledBB){
                BottomNavigationBar(
                    items = listOf(
                        BottomNav(
                            name = "Carnets de compte",
                            route = NavigationRoutes.ACCOUNT_BOOKS,
                            icon = Icons.Filled.AccountBalanceWallet
                        ),
                        BottomNav(
                            name = "Transactions",
                            route = NavigationRoutes.TRANSACTIONS,
                            icon = Icons.Filled.Savings
                        ),
                        BottomNav(
                            name = "Recherche",
                            route = NavigationRoutes.SEARCH,
                            icon = Icons.Default.Search
                        )
                    ),
                    navController = navController,
                    onItemClick = {
                        navController.navigate(it.route)
                    }
                )
            }
        },
        //FIXME SnackBar Appear as many times as we touched, instead of not appearing again when already there
        floatingActionButton = {
            FloatingActionButton(
                //This method is never called idk why, use the below onCLick instead
                onClick = {},
                shape = RoundedCornerShape(50),
                backgroundColor = MaterialTheme.colors.secondary,
                modifier = Modifier.alpha(fbAlpha)
            ) {
                if (enabledFB) {
                    //Use this onClick instead
                    IconButton(onClick = {
                        Log.d(PLUTUS_DEBUG_TAG, "Floating Button touched")
                        val currentRoute = navController.currentBackStackEntry?.destination?.route
                        when(currentRoute){
                            NavigationRoutes.ACCOUNT_BOOKS -> {navController.navigate(NavigationRoutes.ACCOUNT_BOOK_FORM)}
                            NavigationRoutes.TRANSACTIONS -> {navController.navigate(NavigationRoutes.TRANSACTION_FORM)}
                        }
                    }) {
                        Icon(
                            imageVector = Icons.Filled.Add,
                            contentDescription = "Favorite"
                        )
                    }
                }
            }
        },
        backgroundColor = Color.LightGray
    ) {
        Navigation(navController = navController)
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MainPreview(){
    PlutusTheme {
        MyScaffold()
    }
}