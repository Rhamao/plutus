package com.example.plutus.utils.values

import androidx.compose.ui.graphics.Color

class CustomColors {
    companion object {
        val ChalkBlue = Color(0xFF3399FF)
        val ChalkRed = Color(0xFFFF5542)
        val ChalkGreen = Color(0xFF97D077)
        val LighterGray = Color(0xFFDDDDDD)
    }
}
