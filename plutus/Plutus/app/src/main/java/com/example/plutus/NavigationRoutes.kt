package com.example.plutus

class NavigationRoutes {
    companion object {
        const val ACCOUNT_BOOKS = "account_books"
        const val TRANSACTIONS = "transactions"
        const val TRANSACTION = "transaction"
        const val SEARCH = "search"
        private const val FORM = "form"
        const val ACCOUNT_BOOK_FORM = ACCOUNT_BOOKS + "/" + FORM
        const val TRANSACTION_FORM = TRANSACTIONS + "/" + FORM
    }
}