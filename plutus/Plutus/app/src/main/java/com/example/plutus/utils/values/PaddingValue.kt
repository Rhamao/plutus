package com.example.plutus.utils.values

class PaddingValue{
    companion object {
        const val SMALL = 8
        const val SIMPLE = 16
    }
}
