package com.example.plutus.views

import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.plutus.utils.values.PaddingValue
import com.example.plutus.utils.values.TextSizeValue
import com.example.plutus.R
import com.example.plutus.database.TagDB
import com.example.plutus.database.TransactionDB
import com.example.plutus.utils.*
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.models.TextInput
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun TransactionFormView(
    onClickCancel: () -> Unit,
    onClickValidate: (Transaction) -> Unit,
    tagsAdder: (Tag) -> Unit,
    existingTags: List<Tag>,
    alreadyExistingTransaction: Transaction?
) {
    val context = LocalContext.current
    var tags by remember { mutableStateOf(emptyList<Tag>()) }
    val transaction = Transaction.getDefault()
    val nameUpdater: (String) -> Unit = { text: String -> transaction.name = text }
    val amountUpdater: (Float) -> Unit = { value: Float -> transaction.amount = value }
    val dateUpdater: (LocalDate) -> Unit = { date: LocalDate -> transaction.date = date.toString() }
    val descriptionUpdater: (String) -> Unit = { text: String -> transaction.text = text }
    var padding = 2
    var textPadding = padding.dp + 4.dp
    val tagDB = TagDB(context)
    Column(
        Modifier
            .fillMaxHeight()
            .padding(PaddingValue.SIMPLE.dp)
    ) {
        Text(
            text = "Nouvelle transaction",
            fontSize = 26.sp,
            modifier = Modifier.padding(textPadding, 0.dp, 0.dp, 0.dp)
        )
        Column() {
            SimpleTextInput(
                textInput = TextInput("Titre", "Titre..."),
                padding = padding,
                nameUpdater
            )
            NumberTextInput(floatUpdater = amountUpdater, padding = padding)
            SimpleTextInput(
                textInput = TextInput("Description", "Description..."),
                padding = padding,
                descriptionUpdater
            )
            DateInput(padding = padding, dateUpdater = dateUpdater)
        }
        Row(Modifier.padding(textPadding, 0.dp, 0.dp, 0.dp)) {
            Text(text = "Etiquette(s)", fontSize = TextSizeValue.SUB_TITLE_1.sp)
        }
        var width = 100
        var height = 50
        Row(verticalAlignment = Alignment.CenterVertically) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                var tagName = ""
                val tagNameUpdater: (String) -> Unit = { text: String -> tagName = text }
                var tagType: TagType? = null
                val tagTypeUpdater: (String) -> Unit =
                    { text: String -> tagType = stringToTagType(text) }
                Text(
                    text = "Créer : ",
                    fontSize = TextSizeValue.SUB_TITLE_1.sp,
                    textAlign = TextAlign.Center
                )
                TagInput(
                    textInput = TextInput("Nom...", "Nom"),
                    PaddingValue.SIMPLE,
                    stringUpdater = tagNameUpdater,
                    width,
                    height,
                )
                StringSelector(
                    textInput = TextInput("Type..", "Type"),
                    2,
                    stringUpdater = tagTypeUpdater,
                    TagType.values().map { e -> e.name }.toList(),
                    width,
                    height,
                )
                Icon(
                    painter = painterResource(id = R.drawable.new_label),
                    contentDescription = "Tag",
                    Modifier
                        .padding(PaddingValue.SMALL.dp, 0.dp, PaddingValue.SIMPLE.dp, 0.dp)
                        .size(30.dp)
                        .clickable {
                            Log.d(PLUTUS_DEBUG_TAG, "Touched add tag")
                            if (tagName.isNotEmpty() && tagType != null) {
                                val tag = Tag(tagType!!, tagName)
                                if (!tags.contains(tag)) {
                                    Log.d(
                                        PLUTUS_DEBUG_TAG,
                                        "Add tag to list, type : " + tag.type + ", name : " + tag.name
                                    )
                                    tags = tags + listOf(tag)
                                    tagsAdder(tag)
                                } else {
                                    Toast
                                        .makeText(
                                            context,
                                            "Le tag est déjà ajouté",
                                            Toast.LENGTH_LONG
                                        )
                                        .show()
                                }
                            } else {
                                Toast
                                    .makeText(
                                        context,
                                        "Vous devez d'abord remplir les champs Nom et Type.",
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            }
                        }
                )
            }
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            var selectedTag: Tag? = null
            var tagStrings = existingTags.map { e -> e.name }.toList()
            val tagGetter: (String) -> Unit = {
                Log.d(PLUTUS_DEBUG_TAG, "Select an existing tag " + it)
                selectedTag = existingTags.find { tag -> tag.name.equals(it) }
            }
            Text(
                text = "Choisir : ",
                fontSize = TextSizeValue.SUB_TITLE_1.sp,
                textAlign = TextAlign.Center
            )
            StringSelector(
                textInput = TextInput("Choisir..", "Type"),
                PaddingValue.SIMPLE,
                stringUpdater = tagGetter,
                tagStrings,
                width,
                height,
            )
            Icon(
                painter = painterResource(id = R.drawable.new_label),
                contentDescription = "Tag",
                Modifier
                    .padding(PaddingValue.SMALL.dp, 0.dp, PaddingValue.SIMPLE.dp, 0.dp)
                    .size(30.dp)
                    .clickable {
                        Log.d(PLUTUS_DEBUG_TAG, "Touched add tag " + selectedTag)
                        if (selectedTag != null && !tags.contains(selectedTag)) {
                            if (!tags.contains(selectedTag)) {
                                Log.d(
                                    PLUTUS_DEBUG_TAG,
                                    "Add tag to list, type : " + selectedTag!!.type + ", name : " + selectedTag!!.name
                                )
                                tags = tags + listOf(selectedTag!!)
                                tagsAdder(selectedTag!!)
                            } else {
                                Toast
                                    .makeText(
                                        context,
                                        "Le tag est déjà ajouté",
                                        Toast.LENGTH_LONG
                                    )
                                    .show()
                            }
                        } else if (tags.contains(selectedTag)) {

                        } else {
                            Toast
                                .makeText(
                                    context,
                                    "Vous devez d'abord choisir une étiquette",
                                    Toast.LENGTH_LONG
                                )
                                .show()
                        }
                    }
            )
        }
        LazyRow(Modifier.fillMaxWidth()) {
            items(items = tags, itemContent = {
                TagTile(tag = it, padding = 2)
            })
        }
        Row(
            Modifier
                .fillMaxWidth()
                .padding(0.dp, PaddingValue.SMALL.dp, 0.dp, 0.dp)
        ) {
            Button(onClick = { onClickCancel() }) {
                Text(text = "Annuler", fontSize = TextSizeValue.BODY.sp)
            }
            Button(
                onClick = {
                    if (tags.isNotEmpty()) {
                        tags = tags.toMutableList().also { it.removeLast() }
                    }
                },
                modifier = Modifier.padding(2.dp, 0.dp, 0.dp, 0.dp)
            ) {
                Text(text = "Supprimer une étiquette", fontSize = TextSizeValue.BODY.sp)
            }
            Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
                Button(onClick = {
                    if(tags.isEmpty()) {
                        tags = listOf(Tag(TagType.Income, "Revenu"))
                    }
                    transaction.addTags(tags)
                    for (tag in tags) {
                        tagDB.addTag(tag.getID().toString(), tag.type.toString(), transaction.getID(), tag.name);
                    }
                    onClickValidate(transaction)
                }) {
                    Text(text = "Valider", fontSize = TextSizeValue.BODY.sp)
                }
            }
        }

    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyTransactionFormPreview() {
    var transaction = Transaction.getDefault()
    val acb = AccountBook("New one")
    val cancel: () -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "CANCEL") }
    val tagsGetter: (TagType) -> List<Tag> = {
        Log.d(PLUTUS_DEBUG_TAG, it.toString())
        acb.getTags(it).toList()
    }
    val tagsAdder: (Tag) -> Unit = {
        acb.addTag(it)
    }
    val validate: (Transaction) -> Unit = { transac: Transaction ->
        transaction = transac
        Log.d(
            PLUTUS_DEBUG_TAG,
            "VALIDATE " + transaction.date + " " + transaction.text + " " + transaction.name + " " + transaction.amount
        )
    }
    TransactionFormView(cancel, validate, tagsAdder, acb.getAllTagsAsList(), null)
}