package com.example.plutus.views

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.example.plutus.utils.AccountBooksColumn
import com.example.plutus.utils.models.AccountBook
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun AccountBooksView(items: List<AccountBook>, updateSelectedAccountBook: (AccountBook) -> Unit, selectedAccountBookId: String, onClickOnDelete: (AccountBook) -> Unit) {
    AccountBooksColumn(items = items, updateSelectedAccountBook = updateSelectedAccountBook, selectedAccountBookId = selectedAccountBookId, onClickOnDelete = onClickOnDelete)
}

@RequiresApi(Build.VERSION_CODES.O)
@Preview
@Composable
private fun MyAccountBooksViewPreview() {
    val accountBook = AccountBook("Voyage Japon")
    val accountBook2 = AccountBook("Voyage US")
    val accountBook3 = accountBook2.duplicate()
    var list = arrayListOf<AccountBook>()
    val mContext = LocalContext.current
    val foo: (AccountBook) -> Unit = { Log.d(PLUTUS_DEBUG_TAG, "TOUCHED " + it.getName()) }

    for (i in 1..5){
        list.add(accountBook)
    }
    list.add(accountBook2)
    list.add(accountBook3)
    for (i in 1..10){
        list.add(accountBook)
    }

    AccountBooksView(items = list, foo, accountBook2.getUniqueID(), foo)
}
