package com.example.plutus.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.plutus.database.Helper.Helper
import com.example.plutus.utils.models.Transaction
import com.example.plutus.utils.values.PLUTUS_DEBUG_TAG

class TransactionDB(context: Context) {
    private val database: SQLiteDatabase by lazy {
        val helper = Helper(context)
        helper.writableDatabase
    }

    fun getTransactions(accountBookID : String, context : Context) : ArrayList<Transaction> {
        val transactions = ArrayList<Transaction>()
        val query = "SELECT * FROM TransactionTable WHERE AccountBookID = \"$accountBookID\";"
        val cursor = database.rawQuery(query, null)

        while(cursor.moveToNext()) {
            val id = cursor.getString(0);
            val tagDB : TagDB = TagDB(context = context)
            val transaction = Transaction(cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                tagDB.getTags(id),
                cursor.getFloat(4)
            )
            Log.d(PLUTUS_DEBUG_TAG, transaction.name)
            transactions.add(transaction.setTransaction(cursor.getString(0)))

        }
        cursor.close();
        return transactions;
    }

    fun getTransaction(ID: String, context: Context): Transaction {
        val query = "SELECT * FROM TransactionTable WHERE TransactionID = \"$ID\";"
        val cursor = database.rawQuery(query, null)

        val id = cursor.getString(0);
        val tagDB: TagDB = TagDB(context = context)

        return Transaction(
            cursor.getString(1),
            cursor.getString(2),
            cursor.getString(3),
            tagDB.getTags(id),
            cursor.getFloat(4)
        ).setTransaction(id);
    }

    fun removeTransaction(ID : String) : Boolean {
        val cursor = database.delete("TransactionTable", "TransactionID = $ID", null)
        if (cursor > 0)
            return true;
        return false
    }

    fun addTransaction(transaction: Transaction, accountBookID : String) : Boolean {
        val values = ContentValues();
        values.put("TransactionID", transaction.getID());
        values.put("Name", transaction.name);
        values.put("Text", transaction.text);
        values.put("Date", transaction.date);
        values.put("Amount", transaction.amount);
        values.put("AccountBookID", accountBookID);

        val cursor = database.insert("TransactionTable", null, values);

        if(cursor == -1L)
            return false;
        return true;
    }
}